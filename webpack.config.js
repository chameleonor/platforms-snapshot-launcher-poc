"use strict";
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    mode: "development",
    entry: ["babel-polyfill", path.resolve(__dirname, "src", "main.js")],
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist"),
    },
    stats: {
        colors: true,
    },
    resolve: {
        alias: {
            src: path.resolve(__dirname, "src"),
        },
        extensions: [".js", ".jsx", ".css", ".scss"],
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.(s[ac]ss|css)$/,
                loaders: [
                    require.resolve("style-loader"),
                    require.resolve("css-loader"),
                    require.resolve("sass-loader"),
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "public/index.html",
            chunks: [],
            inject: true,
        }),
    ],
};
