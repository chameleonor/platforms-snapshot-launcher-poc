import express from "express";
import path from "path";
import { launch, connect } from "hadouken-js-adapter";

const app = express();

const serverParams = {
    root: path.resolve("./"),
    port: 5555,
    open: false,
    logLevel: 2,
};

const manifestUrl = `http://localhost:${serverParams.port}/manifest`;

app.use(express.json());
app.use(express.static(path.join(__dirname, "src")));
app.use(express.static(path.join(__dirname, "dist")));

/* serves main page  */
app.get("/", (req, res) =>
    res.sendFile("dist/index.html", { root: __dirname })
);

/* manifest */
app.get("/manifest", (req, res) =>
    res.sendFile("src/app.json", { root: __dirname })
);

app.listen(serverParams.port, () =>
    console.log(`server running at ${serverParams.port}`)
);

(async () => {
    try {
        //Once the server is running we can launch OpenFin and retrieve the port.
        const port = await launch({ manifestUrl });

        //We will use the port to connect from Node to determine when OpenFin exists.
        const fin = await connect({
            uuid: "server-connection", //Supply an addressable Id for the connection
            address: `ws://localhost:${port}`, //Connect to the given port.
            nonPersistent: true, //We want OpenFin to exit as our application exists.
        });

        //Once OpenFin exists we shut down the server.
        fin.once("disconnected", process.exit);
    } catch (err) {
        console.error(err);
    }
})();
