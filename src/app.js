import React, { Component } from "react";

class App extends Component {
    constructor(props) {
        //scope functions
        super(props);
        this.getRunningPlatform = this.getRunningPlatform.bind(this);
        this.applyAppSnapshot = this.applyAppSnapshot.bind(this);
        this.launchTradebook = this.launchTradebook.bind(this);
        this.lifecycleSync = this.lifecycleSync.bind(this);
        this.saveSnapshot = this.saveSnapshot.bind(this);
        this.applyStoredSnapshot = this.applyStoredSnapshot.bind(this);
        this.componentNameRandomizer = this.componentNameRandomizer.bind(this);

        this.getRunningPlatform()
            .then(() => this.lifecycleSync())
            .catch(console.error);
    }

    //get the running platorm or start it if it's not.
    async getRunningPlatform() {
        let platform;
        const platformIdentity = {
            uuid: "paltform-1",
        };
        const platformConfiguration = Object.assign(
            {},
            {
                applicationIcon:
                    "https://openfin.github.io/golden-prototype/favicon.ico",
                autoShow: false,
            },
            platformIdentity
        );

        //check if the platform provider application is running.
        if (await fin.Application.wrapSync(platformIdentity).isRunning()) {
            platform = fin.Platform.wrapSync(platformIdentity);
        } else {
            platform = await fin.Platform.start(platformConfiguration);
        }

        return platform;
    }

    //We want to ensure that the platform only closes when the "launcher" closes.
    async lifecycleSync() {
        const platform = await this.getRunningPlatform();
        const pWin = await fin.Application.wrapSync(
            platform.identity
        ).getWindow();
        const launcherWin = await fin.Application.getCurrentSync().getWindow();

        //The Platform provider will try to close if all windows close, we want to prevent this behaviour
        pWin.on("close-requested", () => {
            console.log("platform would have closed, but we prevented it");
        });

        //only close the Platform provider if the "Launcher" closes.
        launcherWin.on("close-requested", () => {
            pWin.close(true);
            launcherWin.close(true);
        });
    }

    //just a wrapper around Platform.applySnapshot that ensures we have the running platform first.
    async applyAppSnapshot(snapshot, close) {
        const p = await this.getRunningPlatform();
        return p.applySnapshot(snapshot, {
            closeExistingWindows: close,
        });
    }

    //We use this function to ensure we can launch multiple components from the same "snapshot"
    componentNameRandomizer() {
        return `component_A${Date.now() + Math.floor(Math.random() * 10000)}`;
    }

    //Snapshot describing a single window with two tabs
    async launchTradebook() {
        const snapshot = {
            windows: [
                {
                    layout: {
                        content: [
                            {
                                type: "stack",
                                id: "no-drop-target",
                                content: [
                                    {
                                        type: "component",
                                        componentName: "view",
                                        componentState: {
                                            name: this.componentNameRandomizer(),
                                            showDevTools: true,
                                            url:
                                                "http://localhost:3000/blotter",
                                        },
                                    },
                                    {
                                        type: "component",
                                        componentName: "view",
                                        componentState: {
                                            name: this.componentNameRandomizer(),
                                            showDevTools: true,
                                            url: "http://localhost:3000/dma",
                                        },
                                    },
                                    {
                                        type: "component",
                                        componentName: "view",
                                        componentState: {
                                            name: this.componentNameRandomizer(),
                                            showDevTools: true,
                                            url:
                                                "http://localhost:3000/summary",
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                },
            ],
        };

        return this.applyAppSnapshot(snapshot, false);
    }

    //we just save the snapshot to localstorage
    async saveSnapshot() {
        const platform = await this.getRunningPlatform();
        const snapshot = await platform.getSnapshot();
        localStorage.setItem("snapShot", JSON.stringify(snapshot));
    }

    //apply the snapshot from localstorage
    async applyStoredSnapshot(e, close) {
        const storedSnapshot = localStorage.getItem("snapShot");
        if (storedSnapshot) {
            this.applyAppSnapshot(JSON.parse(storedSnapshot), close);
        } else {
            throw new Error("No snapshot found in localstorage");
        }
    }

    render() {
        return (
            <div>
                <fieldset>
                    <legend>GBAM WM</legend>
                    <div>
                        <button
                            onClick={() =>
                                this.launchTradebook().catch(console.error)
                            }
                        >
                            Tradebook
                        </button>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Snap shots</legend>
                    <button
                        onClick={() => this.saveSnapshot().catch(console.error)}
                    >
                        Save snapshot
                    </button>
                    <br />
                    <button
                        onClick={(e) =>
                            this.applyStoredSnapshot(e).catch(console.error)
                        }
                    >
                        Apply snapshot
                    </button>
                    <br />
                    <button
                        onClick={(e) =>
                            this.applyStoredSnapshot(e, true).catch(
                                console.error
                            )
                        }
                    >
                        Close Apps and apply snapshot
                    </button>
                    <br />
                </fieldset>
            </div>
        );
    }
}

export default App;
